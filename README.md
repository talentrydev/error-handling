# Error Handling library

This library exposes a service that can be used to convert PHP errors to exceptions.

## Installing and configuring the symfony bundle

* Run:

```
composer require talentrydev/error-handling
```

### Usage

- Get an [ErrorHandler](src/ErrorHandler.php) instance from the [ErrorHandlerFactory](src/Factory/ErrorHandlerFactory.php)
- Call `startHandling()` to start handling errors. You can add as many or as few `Severity` arguments (however at least one is required).
- Execute the code that you expect will throw errors in a try-catch block, catching the appropriate `Throwable` (see table below).
- Call `stopHandling()` to prevent errors being converted to exceptions in other places.

### Example
```
<?php

use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\Error\Warning;
use Talentry\ErrorHandling\ErrorHandler;

class MyClass
{
    public function __construct(
        private readonly ErrorHandler $errorHandler,
    ) {
    }

    public function callBadlyWrittenLibrary()
    {
        try {
            $this->errorHandler->startHandling(new Severity(Severity::WARNING)));
            BadlyWrittenLibrary::methodThatTriggersWarnings();
        } catch (Warning $warning) {
            //log or ignore or do whatever you think is appropriate
        } finally {
            $this->errorHandler->stopHandling();
        }
    }
}		
```


### Available Severity types

According to the [PHP manual](https://secure.php.net/manual/en/function.set-error-handler.php):

> The following error types cannot be handled with a user defined function:
  `E_ERROR`, `E_PARSE`, `E_CORE_ERROR`, `E_CORE_WARNING`, `E_COMPILE_ERROR`, `E_COMPILE_WARNING`,
  and most of `E_STRICT` raised in the file where set_error_handler() is called.

Therefore, this module provides support only for the following Severity types:

| Severity            | PHP Error Level       | Thrown Throwable                                      |
| ------------------- | --------------------- | ----------------------------------------------------- |
| `WARNING`           | `E_WARNING`           | [Warning](src/Error/Warning.php)                      |
| `NOTICE`            | `E_NOTICE`            | [Notice](src/Error/Notice.php)                        |
| `USER_ERROR`        | `E_USER_ERROR`        | [UserError](src/Error/UserError.php)                  |
| `USER_WARNING`      | `E_USER_WARNING`      | [UserWarning](src/Error/UserWarning.php)              |
| `USER_NOTICE`       | `E_USER_NOTICE`       | [UserNotice](src/Error/UserNotice.php)                |
| `STRICT`            | `E_STRICT`            | [Strict](src/Error/Strict.php)                        |
| `RECOVERABLE_ERROR` | `E_RECOVERABLE_ERROR` | [RecoverableError](src/Error/RecoverableError.php)    |
| `DEPRECATED`        | `E_DEPRECATED`        | [Deprecated](src/Error/Deprecated.php)                |
| `USER_DEPRECATED`   | `E_USER_DEPRECATED`   | [UserDeprecated](src/Error/UserDeprecated.php)        |
| `UNKNOWN`           | _None_                | [UnknownError](src/Error/UnknownError.php)            |

The special `UNKNOWN` Severity is provided as a fallback mechanism in case an unrecognized error code is caught.
Do not rely on it or use it directly.

