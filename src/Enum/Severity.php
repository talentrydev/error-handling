<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Enum;

enum Severity: int
{
    case WARNING = 2; //E_WARNING
    case NOTICE = 8; //E_NOTICE
    case USER_ERROR = 256; //E_USER_ERROR
    case USER_WARNING = 512; //E_USER_WARNING
    case USER_NOTICE = 1024; //E_USER_NOTICE
    case STRICT = 2048; //E_STRICT
    case RECOVERABLE_ERROR = 4096; //E_RECOVERABLE_ERROR
    case DEPRECATED = 8192; //E_DEPRECATED
    case USER_DEPRECATED = 16384; //E_USER_DEPRECATED
    case UNKNOWN = -1;
}
