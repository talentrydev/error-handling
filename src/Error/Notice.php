<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Error;

use Talentry\ErrorHandling\Enum\Severity;

class Notice extends Error
{
    public function getSeverity(): Severity
    {
        return Severity::NOTICE;
    }
}
