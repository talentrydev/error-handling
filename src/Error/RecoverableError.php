<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Error;

use Talentry\ErrorHandling\Enum\Severity;

class RecoverableError extends Error
{
    public function getSeverity(): Severity
    {
        return Severity::RECOVERABLE_ERROR;
    }
}
