<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Error;

use Talentry\ErrorHandling\Enum\Severity;
use Exception;

abstract class Error extends Exception
{
    public function __construct(string $message, string $file, int $line)
    {
        $this->message = $message;
        $this->file = $file;
        $this->line = $line;
    }

    abstract public function getSeverity(): Severity;
}
