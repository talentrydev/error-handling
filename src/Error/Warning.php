<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Error;

use Talentry\ErrorHandling\Enum\Severity;

class Warning extends Error
{
    public function getSeverity(): Severity
    {
        return Severity::WARNING;
    }
}
