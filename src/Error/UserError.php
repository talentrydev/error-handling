<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Error;

use Talentry\ErrorHandling\Enum\Severity;

class UserError extends Error
{
    public function getSeverity(): Severity
    {
        return Severity::USER_ERROR;
    }
}
