<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\ErrorHandler;

use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\ErrorHandler;

class NullErrorHandler implements ErrorHandler
{
    public function startHandling(Severity ...$severities): void
    {
    }

    public function stopHandling(): void
    {
    }
}
