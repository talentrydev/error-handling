<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Factory;

use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\Error\Error;
use Talentry\ErrorHandling\Error\Deprecated;
use Talentry\ErrorHandling\Error\Notice;
use Talentry\ErrorHandling\Error\RecoverableError;
use Talentry\ErrorHandling\Error\Strict;
use Talentry\ErrorHandling\Error\UnknownError;
use Talentry\ErrorHandling\Error\UserDeprecated;
use Talentry\ErrorHandling\Error\UserError;
use Talentry\ErrorHandling\Error\UserNotice;
use Talentry\ErrorHandling\Error\UserWarning;
use Talentry\ErrorHandling\Error\Warning;

class ErrorFactory
{
    public function generate(Severity $severity, string $message, string $file, int $line): Error
    {
        switch ($severity) {
            case Severity::WARNING:
                return new Warning($message, $file, $line);
            case Severity::NOTICE:
                return new Notice($message, $file, $line);
            case Severity::USER_ERROR:
                return new UserError($message, $file, $line);
            case Severity::USER_WARNING:
                return new UserWarning($message, $file, $line);
            case Severity::USER_NOTICE:
                return new UserNotice($message, $file, $line);
            case Severity::STRICT:
                return new Strict($message, $file, $line);
            case Severity::RECOVERABLE_ERROR:
                return new RecoverableError($message, $file, $line);
            case Severity::DEPRECATED:
                return new Deprecated($message, $file, $line);
            case Severity::USER_DEPRECATED:
                return new UserDeprecated($message, $file, $line);
            default:
                return new UnknownError($message, $file, $line);
        }
    }
}
