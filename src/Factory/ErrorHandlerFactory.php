<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Factory;

use Talentry\ErrorHandling\ErrorHandler;
use Talentry\ErrorHandling\ErrorHandler\ConvertErrorsToExceptions;

class ErrorHandlerFactory
{
    public function generate(): ErrorHandler
    {
        return new ConvertErrorsToExceptions(new ErrorFactory());
    }
}
