<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling;

use Talentry\ErrorHandling\Enum\Severity;

interface ErrorHandler
{
    /**
     * Start handling errors of the specified Severity (or Severities).
     */
    public function startHandling(Severity ...$severities): void;

    /**
     * Stop handling errors.
     * Call this method only after previously having called startHandling().
     */
    public function stopHandling(): void;
}
