<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Tests\ErrorHandler;

use InvalidArgumentException;
use PHPUnit\Framework\Attributes\WithoutErrorHandler;
use PHPUnit\Framework\TestCase;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\Error\Notice;
use Talentry\ErrorHandling\Error\UserNotice;
use Talentry\ErrorHandling\Error\Warning;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\ErrorHandling\ErrorHandler\ConvertErrorsToExceptions;
use Talentry\ErrorHandling\Factory\ErrorFactory;

class ConvertErrorsToExceptionsTest extends TestCase
{
    private ErrorHandler $handler;
    private string $logErrorsIniValue;
    private string $notice;
    private string $warning;

    protected function setUp(): void
    {
        parent::setUp();

        //by default php errors will be logged to stderr
        //here we expect errors to be triggered in a few tests, so we don't want this behaviour
        $this->logErrorsIniValue = ini_get('log_errors');
        ini_set('log_errors', '0');

        $this->handler = new ConvertErrorsToExceptions(new ErrorFactory());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->handler->stopHandling();
        ini_set('log_errors', $this->logErrorsIniValue);
    }

    #[WithoutErrorHandler]
    public function testHandleWarnings(): void
    {
        $this->handler->startHandling(Severity::WARNING);
        $caught = 0;

        try {
            $this->triggerWarning();
        } catch (Warning $warning) {
            self::assertSame($this->warning, $warning->getMessage());
            $caught++;
        }

        self::assertSame(1, $caught);
    }

    #[WithoutErrorHandler]
    public function testHandleNotices(): void
    {
        $this->handler->startHandling(Severity::NOTICE);

        $caught = 0;

        try {
            $this->triggerNotice();
        } catch (Notice $notice) {
            self::assertSame($this->notice, $notice->getMessage());
            $caught++;
        }

        self::assertSame(1, $caught);
    }

    #[WithoutErrorHandler]
    public function testHandleNoticesAndWarnings(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);

        $caught = 0;

        try {
            $this->triggerWarning();
        } catch (Warning $warning) {
            $caught++;
        }

        try {
            $this->triggerNotice();
        } catch (Notice $notice) {
            $caught++;
        }

        self::assertSame(2, $caught);
    }

    #[WithoutErrorHandler]
    public function testHandleWithZeroArguments(): void
    {
        self::expectException(InvalidArgumentException::class);
        $this->handler->startHandling();
    }

    #[WithoutErrorHandler]
    public function testOnlySpecifiedErrorsAreHandled(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);

        $caught = 0;
        try {
            $this->triggerUserNotice();
        } catch (UserNotice $userNotice) {
            $caught++;
        }

        self::assertSame(0, $caught);
    }

    #[WithoutErrorHandler]
    public function testStopHandling(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);
        $caught = 0;
        try {
            $this->triggerWarning();
        } catch (Warning $warning) {
            $caught++;
        }

        $this->handler->stopHandling();

        try {
            $this->triggerWarning();
        } catch (Warning $warning) {
            $caught++;
        }

        self::assertSame(1, $caught);
    }

    #[WithoutErrorHandler]
    public function testSilencedErrorsAreNotHandled(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);

        $caught = 0;
        try {
            @$this->triggerWarning();
        } catch (Warning $warning) {
            $caught++;
        }

        self::assertSame(0, $caught);
    }

    #[WithoutErrorHandler]
    public function testWrapperErrorHandlerDoesNotHandleUnwantedErrors(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);

        //some libraries will wrap the existing error handler inside their own and delegate calls to it for all errors,
        //regardless of the error_type level specified when registering the error handler
        $prevErrorHandler = set_error_handler(function ($level, $message, $script, $line) use (&$prevErrorHandler) {
            return $prevErrorHandler ? $prevErrorHandler($level, $message, $script, $line) : false;
        });

        $caught = 0;
        try {
            $this->triggerUserNotice();
        } catch (UserNotice $userNotice) {
            $caught++;
        }

        self::assertSame(0, $caught);

        //otherwise PHPUnit complains that "Test code or tested code did not remove its own error handlers"
        restore_error_handler();
    }

    #[WithoutErrorHandler]
    public function testNestedCalls(): void
    {
        $this->handler->startHandling(Severity::NOTICE, Severity::WARNING);
        $this->handler->startHandling(Severity::USER_NOTICE);

        $caught = 0;

        try {
            $this->triggerWarning();
        } catch (Warning $userNotice) {
            self::fail('Warning should not be handled');
        }

        try {
            $this->triggerUserNotice();
        } catch (UserNotice $userNotice) {
            $caught++;
        }

        $this->handler->stopHandling();

        try {
            $this->triggerUserNotice();
        } catch (UserNotice $userNotice) {
            self::fail('UserNotice should not be handled');
        }

        try {
            $this->triggerWarning();
        } catch (Warning $userNotice) {
            $caught++;
        }

        $this->handler->stopHandling();

        try {
            $this->triggerWarning();
        } catch (Warning $userNotice) {
            self::fail('Warning should not be handled');
        }

        self::assertSame(2, $caught);
    }

    private function triggerNotice(): void
    {
        $this->notice = 'Object of class stdClass could not be converted to int';
        (object) [1] == 1;
    }

    private function triggerWarning(): void
    {
        $this->warning = 'Trying to access array offset on int';
        $s = 1;
        $s[1];
    }

    private function triggerUserNotice(): void
    {
        trigger_error('User notice', E_USER_NOTICE);
    }
}
