<?php

declare(strict_types=1);

namespace Talentry\ErrorHandling\Tests\Factory;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\Error\Deprecated;
use Talentry\ErrorHandling\Error\Notice;
use Talentry\ErrorHandling\Error\RecoverableError;
use Talentry\ErrorHandling\Error\Strict;
use Talentry\ErrorHandling\Error\UnknownError;
use Talentry\ErrorHandling\Error\UserDeprecated;
use Talentry\ErrorHandling\Error\UserError;
use Talentry\ErrorHandling\Error\UserNotice;
use Talentry\ErrorHandling\Error\UserWarning;
use Talentry\ErrorHandling\Error\Warning;
use Talentry\ErrorHandling\Factory\ErrorFactory;

class ErrorFactoryTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testGenerateError(Severity $severity, string $expectedClass): void
    {
        $factory = new ErrorFactory();
        $error = $factory->generate($severity, 'message', 'file', 1);

        self::assertInstanceOf($expectedClass, $error);
        self::assertSame('message', $error->getMessage());
        self::assertSame('file', $error->getFile());
        self::assertSame(1, $error->getLine());
    }

    public static function dataProvider(): array
    {
        return [
            [Severity::DEPRECATED, Deprecated::class],
            [Severity::NOTICE, Notice::class],
            [Severity::RECOVERABLE_ERROR, RecoverableError::class],
            [Severity::STRICT, Strict::class],
            [Severity::USER_DEPRECATED, UserDeprecated::class],
            [Severity::USER_ERROR, UserError::class],
            [Severity::USER_NOTICE, UserNotice::class],
            [Severity::USER_WARNING, UserWarning::class],
            [Severity::WARNING, Warning::class],
            [Severity::UNKNOWN, UnknownError::class],
        ];
    }
}
