test:
	docker run -t -w /app -v $(shell pwd):/app --rm php:8.3-cli bash -c 'cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini && vendor/bin/phpunit'
cs:
	docker run -t -w /app -v $(shell pwd):/app --rm php:8.3-cli bash -c 'vendor/bin/phpcs --standard=PSR12 src tests'
cs-fix:
	docker run -t -w /app -v $(shell pwd):/app --rm php:8.3-cli bash -c 'vendor/bin/phpcbf --standard=PSR12 src tests'
deps:
	docker run -v $(shell pwd):/app --rm -t composer install
